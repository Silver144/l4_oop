package com.kishkan91.Items;

public interface Book {
    public String getBookName();
    public String getAuthorName();
}
